package com.hussainmehboob.gitdemoapp.retrofitservices

import com.hussainmehboob.gitdemoapp.models.response.GitUserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface GitUserService {
    @GET("search/users")
    suspend fun getAllGitUsers(@Header("authorization") authorization:String, @Query("q", encoded = true) query:String, @Query("page", encoded = true) pageNumber : Int,
                               @Query("per_page", encoded = true) perPage : Int): Response<GitUserResponse>
}