package com.hussainmehboob.gitdemoapp.paging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hussainmehboob.gitdemoapp.BR
import com.hussainmehboob.gitdemoapp.databinding.ItemGitUserBinding
import com.hussainmehboob.gitdemoapp.models.response.GitUser
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GitUserPagingAdapter @Inject constructor() : PagingDataAdapter<GitUser, GitUserPagingAdapter.MyViewHolder>(DIFF_UTIL) {

    var onCLick: ((GitUser) -> Unit)? = null

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<GitUser>() {
            override fun areItemsTheSame(oldItem: GitUser, newItem: GitUser): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: GitUser, newItem: GitUser): Boolean {
                return oldItem == newItem
            }
        }
    }

    fun onGitUserClick(listener: (GitUser) -> Unit) {
        onCLick = listener
    }

    inner class MyViewHolder(val viewDataBinding: ItemGitUserBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = getItem(position)

        holder.viewDataBinding.setVariable(BR.gitUser, data)

        holder.viewDataBinding.root.setOnClickListener {
            onCLick?.let {
                it(data!!)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemGitUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }
}