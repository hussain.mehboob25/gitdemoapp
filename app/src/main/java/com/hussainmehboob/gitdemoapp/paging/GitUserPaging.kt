package com.hussainmehboob.gitdemoapp.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.hussainmehboob.gitdemoapp.models.response.GitUser
import com.hussainmehboob.gitdemoapp.retrofitservices.GitUserService
import com.hussainmehboob.gitdemoapp.utils.Constants
import com.hussainmehboob.gitdemoapp.utils.Constants.perPageUsers
import com.hussainmehboob.gitdemoapp.utils.EspressoIdlingResource

class GitUserPaging (val gitUserService: GitUserService) : PagingSource<Int, GitUser>() {

    override fun getRefreshKey(state: PagingState<Int, GitUser>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GitUser> {
        EspressoIdlingResource.increment()
        //Handling the paging stuff
        val page = params.key ?: 1

        return try {
                val data = gitUserService.getAllGitUsers(Constants.authToken, Constants.queryString, page, perPageUsers)
                Log.d("TAG", "load: ${data.body()}")

            when(data.code()) {
                422 -> {
                    LoadResult.Error(Throwable("Validation failed"))
                }
                503 -> {
                    LoadResult.Error(Throwable("Service unavailable"))
                }
                else -> {
                    LoadResult.Page(
                        data = data.body()?.items!!,
                        prevKey = if (page == 1) null else page - 1,
                        nextKey = if (data.body()?.items?.isEmpty()!!) null else page + 1
                    )
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            LoadResult.Error(e)
        } finally {
            EspressoIdlingResource.decrement()
        }
    }
}