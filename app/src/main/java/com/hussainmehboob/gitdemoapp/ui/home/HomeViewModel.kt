package com.hussainmehboob.gitdemoapp.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.hussainmehboob.gitdemoapp.paging.GitUserPaging
import com.hussainmehboob.gitdemoapp.retrofitservices.GitUserService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val gitUserService: GitUserService
) : ViewModel()  {

    val list = Pager(PagingConfig(pageSize = 5)) {
        GitUserPaging(gitUserService)
    }.liveData.cachedIn(viewModelScope)

}