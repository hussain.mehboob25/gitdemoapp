package com.hussainmehboob.gitdemoapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.hussainmehboob.gitdemoapp.R
import com.hussainmehboob.gitdemoapp.databinding.FragmentHomeBinding
import com.hussainmehboob.gitdemoapp.paging.GitUserPagingAdapter
import com.hussainmehboob.gitdemoapp.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    lateinit var fragmentHomeBinding: FragmentHomeBinding

    @Inject
    lateinit var gitUserAdapter : GitUserPagingAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)

        return fragmentHomeBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setRecyclerView()

        //Handling the Item click
        gitUserAdapter?.onGitUserClick {
            val bundle = bundleOf(Constants.ARGS_GIT_USER to it)
            findNavController().navigate(R.id.action_nav_home_to_nav_details, bundle)
        }

        //Submit the changed to paging adapter when list gets updated
        homeViewModel.list.observe(viewLifecycleOwner) {
            gitUserAdapter?.submitData(lifecycle, it)
            fragmentHomeBinding.progressBar.visibility = View.GONE
            fragmentHomeBinding.rvGitUser.visibility = View.VISIBLE
        }

        fragmentHomeBinding.refreshLayout.setOnRefreshListener {
            gitUserAdapter?.refresh()
            fragmentHomeBinding.refreshLayout.isRefreshing = false
        }

    }

    private fun setRecyclerView() {
        fragmentHomeBinding.rvGitUser.apply {
            adapter = gitUserAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }
}