package com.hussainmehboob.gitdemoapp.ui.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.hussainmehboob.gitdemoapp.databinding.FragmentDetailsBinding
import com.hussainmehboob.gitdemoapp.models.response.GitUser
import com.hussainmehboob.gitdemoapp.utils.Constants
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    lateinit var fragmentDetailsBinding: FragmentDetailsBinding

    private lateinit var detailsViewModel: DetailsViewModel

    lateinit var gitUser : GitUser

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        detailsViewModel = ViewModelProvider(this).get(DetailsViewModel::class.java)
        fragmentDetailsBinding = FragmentDetailsBinding.inflate(inflater, container, false)
        gitUser = requireArguments().getSerializable(Constants.ARGS_GIT_USER) as GitUser

        fragmentDetailsBinding.gitUser = gitUser

        //Handling the click on web link
        fragmentDetailsBinding.llGitUserUrl.setOnClickListener {
            val gitUserUrl = fragmentDetailsBinding.txtGitUserUrl.text.toString()
            if (gitUserUrl != "") {
                requireView().context.startActivity(
                        Intent(Intent.ACTION_VIEW, Uri.parse(gitUserUrl))
                )
            }
        }

        return fragmentDetailsBinding.root

    }
}