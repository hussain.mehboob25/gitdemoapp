package com.hussainmehboob.gitdemoapp.utils

object Constants {
    const val BASE_URL = "https://api.github.com/"
    const val queryString = "repos:%3E42+followers:%3E1000"
    const val perPageUsers = 10
    const val authToken = "token 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"

    //Bundle Argument
    const val ARGS_GIT_USER = "gitUser"
}