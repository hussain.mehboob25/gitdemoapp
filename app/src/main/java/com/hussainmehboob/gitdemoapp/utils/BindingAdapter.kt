package com.hussainmehboob.gitdemoapp.utils

import android.R
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("userImage")
fun loadGitUserImage(view: ImageView, url: String) {

    url.let {
        val options: RequestOptions = RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.ic_menu_report_image)
            .error(R.drawable.ic_menu_report_image)
            .priority(Priority.HIGH)
        Glide.with(view)
            .load(it)
            .apply(options)
            .into(view)
    }


}