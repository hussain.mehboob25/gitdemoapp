package com.hussainmehboob.gitdemoapp

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ApplicationClass : MultiDexApplication() {
}