package com.hussainmehboob.gitdemoapp.models.response

import java.io.Serializable

data class GitUserResponse(
    val incomplete_results: Boolean,
    val items: List<GitUser>,
    val total_count: Int
): Serializable