package com.hussainmehboob.gitdemoapp.ui.home

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.hussainmehboob.gitdemoapp.MainActivity
import com.hussainmehboob.gitdemoapp.R
import com.hussainmehboob.gitdemoapp.data.TestData
import com.hussainmehboob.gitdemoapp.paging.GitUserPagingAdapter
import com.hussainmehboob.gitdemoapp.utils.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class HomeFragmentTest{

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    val LIST_ITEM_IN_TEST = 4
    val USER_IN_TEST = TestData.gitUsers[LIST_ITEM_IN_TEST]


    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }


    @Test
    fun test_isHomeFragmentVisible_onAppLaunch() {
        Espresso.onView(withId(R.id.rvGitUser)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_selectListItem_isDetailFragmentVisible() {

        // Click list item #LIST_ITEM_IN_TEST
        Espresso.onView(withId(R.id.rvGitUser))
            .perform(actionOnItemAtPosition<GitUserPagingAdapter.MyViewHolder>(LIST_ITEM_IN_TEST, ViewActions.click()))

        // Confirm nav to DetailFragment and display title
        Espresso.onView(withId(R.id.gituser_name))
            .check(ViewAssertions.matches(withText(USER_IN_TEST.login)))
    }

    @Test
    fun test_backNavigation_toHomeFragment() {

        // Click list item #LIST_ITEM_IN_TEST
        Espresso.onView(withId(R.id.rvGitUser))
            .perform(actionOnItemAtPosition<GitUserPagingAdapter.MyViewHolder>(LIST_ITEM_IN_TEST, ViewActions.click()))

        // Confirm nav to DetailFragment and display title
        Espresso.onView(withId(R.id.gituser_name))
            .check(ViewAssertions.matches(withText(USER_IN_TEST.login)))

        Espresso.pressBack()

        // Confirm HomeFragment in view
        Espresso.onView(withId(R.id.rvGitUser))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}