package com.hussainmehboob.gitdemoapp

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    //This is a sample unit test
    //The application doesn't have other calculations or anything for unit testing.
    //Please view UI tests
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}