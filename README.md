# Git Demo App

Git Users Search Native Android App

## Pre-Requisites

Use Java 11 for setting up this project as we are using latest hilt-android version 2.40.5 which requires it

## Getting started

Setup the project using the command "git clone https://gitlab.com/hussain.mehboob25/gitdemoapp.git"


## Components used
MVVM Architecture, 
Jetpack, 
Kotlin, 
LiveData, 
Dagger-Hilt, 
Retrofit, 
Glide, 
Paging, 
Circular Image View, 
Androidx, 
Navigation Graph, 
Constraint Layout,
SwipeRefreshLayout

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hussain.mehboob25/gitdemoapp.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://gitlab.com/hussain.mehboob25/gitdemoapp/-/settings/integrations)

## Collaboration (Currently not available)

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:f2bbeb24fa121a11d473cc24c3faa758?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Tests can be run from the following two files, each file contains the respective tests:

DetailsFragmentTest => UI Test Class
HomeFragmentTest => Unit Test Class

Debug Build is available on the below link for viewing the app functionality:

https://drive.google.com/file/d/1wotUJQ197cS2QmEQg3k8LY8Hww0Df9QZ/view?usp=sharing

***

## Usage
Could be used to get started with RecyclerViews using MVVM Architecture

## Support
Contact the developer @ hussain_mehboob@hotmail.com

## Contributing
Currently this project is private and for assessment purpose only.

## Authors and acknowledgment
Author: Hussain Mehboob
Credits: Git Search API

## License
Open Source Project

